# OpenML dataset: Diabetes

https://www.openml.org/d/41425

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This data was collected from combine primary and secondary sources, through questionnaire, verbal interview and some part of the hospital&rsquo;s record department&rsquo;s data, from the selected government&rsquo;s hospitals in the Northwestern states of Nigeria. The hospitals include: Ajingi general hospital (Kano State), Murtala Muhammad specialist hospital  (Kano State), Abdullahi Wase specialist hospital  (Kano State), Sir Muhammad Sunusi general hospital (Kano State), Gaya general hospital (Kano State), Gwarzo general hospital (Kano State), Federal medical centre Birnin-kudu (Jigawa State), Hadejia general hospital (Jigawa State), Gumel general hospital (Jigawa State), Katsina general hospital (Katsina State), Daura general hospital (Katsina State), Malumfashi general hospital (Katsina State), Gusau general hospital (Zamfara State), Talata Mafara general hospital (Zamfara State), Kaura Namoda general hospital (Zamfara State) and Bodinga general hospital (Sokoto State).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41425) of an [OpenML dataset](https://www.openml.org/d/41425). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41425/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41425/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41425/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

